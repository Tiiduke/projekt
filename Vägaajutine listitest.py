from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from time import sleep
from opponents_and_cards import *
from PIL import Image, ImageTk  #Vajalik Jura

###See on programm, mis näitab juhendaja petmiseks mittetöötavat GUI-d.
def kaardinäitaja(x): #Võtab suurest pildist välja kindla kaardi.#Kaardid on indekseeritud nii, et x = väärtused 0-12 ja y = mast (0-3 e. risti,ruutu,ärtu,poti)
    y = x[1]
    x = x[0]
    if x < 12:
        x1 = (x+1)*79    #Ülemise vasaku nurga kaugus pildi lääneservast.
        y1 = y*123       #Ülemise vasaku nurga kaugus pildi põhjaservast.
        x2 = (x+2)*79    #Alumise parema nurga kaugus pildi lääneservast.
        y2 = (y+1)*123   #Alumise parema nurga kaugus pildi põhjaservast.
    elif x == 12:
        x1 = 0
        y1 = 123*y
        x2 = 79
        y2 = 123*(y+1)
    elif x == -1:
        x1 = 79*2
        y1 = 123*4
        x2 = 79*3
        y2 = 123*5
    else:
        x1 = 79*3
        y1 = 123*4
        x2 = 79*4
        y2 = 123*5

    pilt = Image.open("cards.png")       #Võtame kaardipildi ja lõikame õige jupi.
    crop_rectangle = (x1, y1, x2, y2)
    cropped_pilt = pilt.crop(crop_rectangle)
    
    return ImageTk.PhotoImage(cropped_pilt) #Tadaaa! Pilt käes


raam = Tk()                            #Teeme ühe kasutajaliidese ja paneme kaardinäitajaga sinna pildi.
raam.title("Ahjuroop")
raam.config(bg="green")

#5 KESKMIST KAARTI
väikeraam = Frame()
väikeraam.grid(column=3, row=3, padx=10, pady=10, columnspan=3)
väikeraam.config(bg="green")

table_images = []
for item in table:
    table_images.append(kaardinäitaja(item))

laud = []
for i in range(0, len(table_images)):
    laud.append(Label(väikeraam, image=table_images[i], bg="green").grid(column=i+1, row=2, sticky=(N, W)))



bm = Text(väikeraam, height=1, width=10)
bm.grid(column=0, row=1, columnspan=len(table)+1)

#Inimene kohal [0], players[i].hand
def teeraam():
    return Frame(bg="green") #See on iga raami baas.

def mkaart(raamnr, mängijanr, kaardinr): #See paneb vastavasse raami vastava pildi
    global players, player_cards
    if player_cards[mängijanr] == []:
        kaart = kaardinäitaja((100, 8))
        return Label(raamnr, image=kaart, bg="green")
    else:
        kaardid = player_cards[mängijanr]
        kaart = kaardid[kaardinr]
        return Label(raamnr, image=kaart, bg="green")

player_cards = []
for item in players:
    temp = []
    if item.hand == []:
        temp.append(kaardinäitaja((100,100)))
        temp.append(kaardinäitaja((100,100)))
        player_cards.append(temp)
    else:
        for card in item.hand:
            temp.append(kaardinäitaja(card))
        player_cards.append(temp)

def column(i):
    if i == 0: return 3
    elif i == 1 or i == 2: return 1
    elif i == 3: return 2
    elif i == 4: return 3
    elif i == 5 or i == 6 or i == 7: return i
    elif i == 8: return 7
def row(i):
    if i == 0: return 4
    elif i == 1: return 4
    elif i == 2: return 3
    elif i == 3: return 2
    elif i == 4: return 1
    elif i >= 5 and i <= 8: return i - 4
def columnspan(i):
    if i == 0: return 3
    else: return 1

display_players = []

for i in range(0, len(player_cards)):
    mraam = teeraam()
    mraam.grid(column=column(i), row=row(i), padx=10, pady=10, columnspan=columnspan(i))
    display_players.append(mkaart(mraam, i, 0).grid(column=1, row=row(i), sticky=(W)))
    display_players.append(mkaart(mraam, i, 1).grid(column=2, row=row(i), sticky=(W)))
    text = Text(mraam, height=1, width=10, state=NORMAL)
    text.grid(column=1, row=0, columnspan=2)
    text.insert(INSERT, players[i].name)
    display_players.append(text.config(state=DISABLED))
    display_players.append(Text(mraam, height=1, width=10).grid(column=1, row=5))
    display_players.append(Text(mraam, height=1, width=10).grid(column=2, row=5))


f = 2 #See on CALL nupu jaoks tehtud jura.
#NUPUD
nupuraam = teeraam()
nupuraam.grid(column=3,row=5, padx=10, pady=10, columnspan=3)
nupukas =[[Button (nupuraam, text="FOLD").grid(column=1,row=1)],\
          [Button (nupuraam, text="CALL"+ " (" + str(f) + ")").grid(column=2,row=1)],\
          [Button (nupuraam, text="RAISE").grid(column=3,row=1)],\
          [Entry(nupuraam).grid(column=4, row=1)]]
raam.mainloop()