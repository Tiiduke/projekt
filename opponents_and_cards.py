__author__="Tiit Hendrik Piibeleht"

from random import randint
from hand_categories import *
from hands_and_values import *
#from time import sleep
from GUI import *

#Moodustab 52st ennikust koosneva kaardipaki
def deck():
    deck = []
    for i in range(0,13):
        for j in range(0,4):
            deck.append((i, j))
    return deck
#Jagab juhuslikult pakist kaardi ja siis tagastab vastava kaardi, eemaldades kaardi pakist
def deal_card(deck):
    card = deck.pop(randint(0, len(deck)-1))
    return card
#Loob nullidest koosneva järjendi vastavalt argumendile
def empty_pot(count):
    bets = []
    for i in range (0, count):
        bets.append(0)
    return bets

played_hands = 0
tempo = 10
#tempo = int(input("Kui mitme käe tagant tõusevad blindid? Sisesta täisarvuline väärtus."))

#Funktsioon mis määrab BB suuruse
def big_blind():
    global played_hands, tempo
    counter = played_hands//tempo
    if counter == 0: return 20
    elif counter == 1: return 30
    elif counter == 2: return 40
    elif counter == 3: return 60
    elif counter == 4: return 80
    elif counter == 5: return 100
    elif counter == 6: return 150
    elif counter == 7: return 200
    elif counter == 8: return 250
    elif counter == 9: return 300
    elif counter > 9: return 400
#Jagab lauakaardi vastavalt hetke laua seisule(flop/turn/river)
def deal_table():
    global table, players, pack
    if len(table)==0:
        while len(table) < 3:
            table.append(deal_card(pack))
    elif len(table)==3:
        table.append(deal_card(pack))
    elif len(table)==4:
        table.append(deal_card(pack))
    elif len(table)==5:
        table = []
    for item in players:
        item.current_bet = 0
#Funktsioon hetkel alles olevate käte arvu saamiseks
def hand_count():
    global players
    i = 0
    for item in players:
        if item.hand != []:
            i += 1
    return i
#Funktsioon raundi läbimängimiseks
def round():
    global players, table, temp_pot, pot, raam
    if len(players) == 2:
        for item in players:
            if item.position == 0:
                max_bet = players.index(item)
                item.make_bet(big_blind())
            elif item.position == 1:
                item.make_bet(big_blind()//2)
                i = players.index(item)
    for item in players:
        if item.position == len(players) - 2:
            max_bet = players.index(item)
            item.make_bet(big_blind())
        elif item.position == len(players) - 1:
            item.make_bet(big_blind()//2)
        elif item.position == len(players) - 3:
            i = players.index(item)
    while True:
        while (max_bet != i%(len(players)) and max(temp_pot) != 0) or (i > -2):
            #sleep(0.5)
            print(temp_pot)
            if players[i%len(players)].hand == []:
                pass
            elif players[i%len(players)].char == "AI" and players[i%len(players)].chips != 0:
                players[i%len(players)].computer_bet()
                if players[i%len(players)].current_bet > players[max_bet].current_bet:
                    max_bet = i%len(players)
            elif players[i%len(players)].char == "human" and players[i%len(players)].chips !=0:
                players[i%len(players)].human_bet()
                if players[i%len(players)].current_bet > players[max_bet].current_bet:
                    max_bet = i%len(players)
            else: break
            i -= 1
        #sleep(0.5)
        if hand_count() == 1:
            break
        if len(table) == 5:
            break
        deal_table()
        raam = gui(players, table)
        max_bet = 0
        i = len(players) - 1
        temp_pot = empty_pot(len(players))
    if len(table) == 5 and hand_count() != 1:
        for item in players:
            if item.hand != []:
                item.chips += pot
            item.current_bet = 0
    elif len(table) == 5:
        winner()
    elif len(table) != 5 and hand_count() == 1:
        table = []
        for item in players:
            if item.hand != []:
                item.chips += pot
            item.current_bet = 0
#Funktsioon showdownil võitja leidmiseks
def winner():
    global players, table, pot
    i = 0
    hand_values = []
    for item in players:
        if item.hand != []:
            item.hand_value = table_hand_cards(item.hand, table)
            hand_values.append(item.hand_value)
    best_hand = better_hand(hand_values)
    for item in players:
        if item.hand_value == best_hand:
            i += 1
    for item in players:
        if item.hand_value == best_hand:
            item.chips += pot/i


#Funktsioon, mis ütleb, kas mängijate järjendis on kaotanud mängijaid
def zero_chips():
    global players
    for item in players:
        if item.chips == 0:
            return True
    return False
#Funktsioon kõige nullimiseks
def clean_board():
    global pack, players, table, temp_pot, pot, played_hands
    k = 0
    for i in range(0, len(players)):
        players[i].hand = []
        #players[i].chips += players[i].current_bet
        players[i].current_bet = 0
        players[i].hand_value = []
        players[i].change_position()
    pack = deck()
    table = []
    temp_pot = empty_pot(len(players))
    pot = 0
    played_hands += 1
    while zero_chips():
        if players[k%len(players)].chips == 0:
            pos = players[k%len(players)].position
            for thing in players:
                if thing.position > pos:
                    thing.position -= 1
            players.pop(k%len(players))
        k += 1
#Funktsioon kõigile mängijatele käte jagamiseks
def deal_hands():
    global players
    for item in players:
        item.deal_cards()
#Funktsioon mängu jooksutamiseks :)
def game():
    global pack, players, raam, table
    while len(players) > 1:
        deal_hands()
        raam = gui(players, table)
        round()
        clean_board()
        print(len(table))
#Loon klassi mängija jaoks mis sisaldaks järgmisi elemente: nimi, chipide hulk, käsi, positsioon lauas
class Player:
    def __init__(self, name, chips, hand, position, current_bet, char):
        self.name = name
        self.chips = chips
        self.hand = hand
        self.position = position
        self.current_bet = current_bet
        self.char = char
        self.hand_value = []
    #Meetod stacki suuruse määramiseks
    def stack_position(self):
        global players
        i = 0
        for item in players:
            if item.chips > self.chips:
                i += 1
        return i
    #Meetod arvutile panuse tegemiseks
    def computer_bet(self):
        global temp_pot, pot, players, opponents
        if self.amount() >= self.chips:
            self.make_bet(self.chips)
        elif self.current_bet == max(temp_pot) and self.amount() == 0:
            self.make_bet(self.amount())
        elif max(temp_pot) > 0 and self.amount() == 0:
            self.fold_cards()
        elif self.amount() > max(temp_pot):
            if max(temp_pot) > 0:
                if self.chips/max(temp_pot) < 3:
                    self.make_bet(self.chips)
                else: self.make_bet(self.amount())
            else: self.make_bet(self.amount())
        else:
            self.make_bet(self.amount())
    #Meetod inimesele panuse tegemiseks
    def human_bet(self):
        global temp_pot, pot
        bet = int(input("Tee panus"))
        if bet + self.current_bet >= self.chips:
            self.make_bet(self.chips)
        elif bet + self.current_bet < max(temp_pot):
            self.fold_cards()
        else: self.make_bet(bet)
    #Meetod panuse tegemiseks
    def make_bet(self, bet):
        global pot, temp_pot
        temp_pot[self.position] += bet
        self.chips -= bet
        self.current_bet += bet
        pot += bet
    #Meetod positsiooni vahetamiseks
    def change_position(self):
        if self.position == len(players) - 1: self.position = 0
        else: self.position += 1
    #Meetod kaartide jagamiseks
    def deal_cards(self):
        cards = []
        cards.append(deal_card(pack))
        cards.append(deal_card(pack))
        self.hand = cards
    #Meetod kaartidest loobumiseks
    def fold_cards(self):
        self.hand = []
    def amount(self):
        global pot, temp_pot, table, players
        if big_blind() < 60 and len(players) > 4:
            if len(table) == 0:
                if category(self.hand) > 4: return 0
                elif category(self.hand) < 3: return 4*max(temp_pot)
                elif category(self.hand) < 5 and self.position < 2:
                    if max(temp_pot) > big_blind(): return 0
                    else: return 3*big_blind()
                else: return 0
            elif len(table) >= 3 and strong_made(self.hand, table): return pot
            elif len(table) < 5 and (medium_made_draw(self.hand, table) or pot_odds(self.hand, table, max(temp_pot), pot)): return max(temp_pot) - self.current_bet - self.current_bet
            elif len(temp_pot) == 2 and medium_made_draw(self.hand, table): return 3*big_blind()
            else: return 0
        elif big_blind() > 40 and len(players) > 4:
            if len(table) == 0:
                if self.chips > 10*big_blind():
                    if category(self.hand) < 8 and temp_pot.count(big_blind()) == 1 \
                        and max(temp_pot) == big_blind(): return 4*big_blind()
                    elif category(self.hand) < 5 and temp_pot.count(big_blind()) > 1: return (3+temp_pot.count(big_blind()))*big_blind()
                    elif category(self.hand) < 4 and max(temp_pot) > big_blind():
                        if pot/max(temp_pot) >= 2 and category(self.hand) < 4: return max(temp_pot) - self.current_bet
                        elif pot/max(temp_pot) >= 1.5 and category(self.hand) < 3: return max(temp_pot) - self.current_bet
                        else: return 0
                    else: return 0
                elif self.chips <= 10*big_blind():
                    if category(self.hand) < 9 and temp_pot.count(big_blind()) == 1 \
                        and max(temp_pot) == big_blind(): return 4*big_blind()
                    elif category(self.hand) < 6 and temp_pot.count(big_blind()) > 1: return (3+temp_pot.count(big_blind()))*big_blind()
                    elif category(self.hand) < 5 and max(temp_pot) > big_blind():
                        if pot/max(temp_pot) >= 2 and category(self.hand) < 5: return max(temp_pot) - self.current_bet
                        elif pot/max(temp_pot) >= 1.5 and category(self.hand) < 4: return max(temp_pot) - self.current_bet
                        elif pot/max(temp_pot) >= 1 and category(self.hand) < 3: return max(temp_pot) - self.current_bet
                        else: return 0
                    else: return 0
            elif len(table) >= 3:
                if self.position == len(players) - 2:
                    if medium_made_draw(self.hand, table): return pot
                    elif draw(self.hand, table) > 0.17 and max(temp_pot) >= big_blind(): return max(temp_pot) - self.current_bet
                    else: return 0
                elif len(temp_pot) == 2:
                    if medium_made_draw(self.hand, table): return self.chips
                    elif (draw(self.hand, table) > 0.17 or table_hand_cards(self.hand, table)[0] == 1) and max(temp_pot)==0: return pot
                    else: return 0
                elif len(temp_pot) > 2:
                    if medium_made_draw(self.hand, table): return self.chips
                    elif draw(self.hand, table) > 0.17 and max(temp_pot) < 0.2*self.chips: return max(temp_pot) - self.current_bet
                    else: return 0
                else: return 0
            elif len(table) == 5:
                if table_hand_cards(self.hand, table)[0] >= 3: return self.chips
                elif table_hand_cards(self.hand, table)[0] >= 1: return max(temp_pot) - self.current_bet
                else: return 0
            else: return 0
        elif len(players) == 4:
            if len(table) == 0:
                if self.stack_position() == 0 and pot == 1.5*big_blind() and category(self.hand) <= 8: return self.chips
                elif self.stack_position() >= 1 and pot == 1.5*big_blind() and category(self.hand) <= 5: return self.chips
                elif pot > 1.5*big_blind() and self.stack_position() == 0:
                    if max(temp_pot) >= 10*big_blind() and category(self.hand) <= 2: return max(temp_pot) - self.current_bet
                    elif max(temp_pot) >= 8*big_blind() and category(self.hand) <= 3: return max(temp_pot) - self.current_bet
                    elif max(temp_pot) >= 6*big_blind() and category(self.hand) <= 4: return max(temp_pot) - self.current_bet
                    elif max(temp_pot) >= 4*big_blind() and category(self.hand) <= 5: return max(temp_pot) - self.current_bet
                    else: return 0
                elif pot > 1.5*big_blind() and self.stack_position() >= 1:
                    if max(temp_pot) >= 10*big_blind() and category(self.hand) == 1: return max(temp_pot) - self.current_bet
                    elif max(temp_pot) >= 8*big_blind() and category(self.hand) == 1: return max(temp_pot) - self.current_bet
                    elif max(temp_pot) >= 6*big_blind() and category(self.hand) <= 2: return max(temp_pot) - self.current_bet
                    elif max(temp_pot) >= 4*big_blind() and category(self.hand) <= 3: return max(temp_pot) - self.current_bet
                    else: return 0
                else: return 0
            elif len(table) >= 3:
                if max(temp_pot) == 0 and medium_made_draw(self.hand, table): return 3*big_blind()
                elif max(temp_pot) > 0 and strong_made(self.hand, table): return 3*max(temp_pot)
                else: return 0
        elif len(players) == 3:
            if len(table) == 0:
                if self.stack_position() == 1 and category(self.hand) <= 2 and max(temp_pot)>= 10*big_blind(): return self.chips
                elif self.stack_position() == 1 and category(self.hand) <= 3 and max(temp_pot)>= 8*big_blind(): return self.chips
                elif self.stack_position() == 1 and category(self.hand) <= 4 and max(temp_pot)>= 6*big_blind(): return self.chips
                elif self.stack_position() == 1 and category(self.hand) <= 5 and max(temp_pot)>= 4*big_blind(): return self.chips
                elif self.stack_position() == 1 and category(self.hand) <= 5 and max(temp_pot) == big_blind(): return self.chips
                elif self.stack_position()%2 == 0 and category(self.hand) <= 2 and max(temp_pot)>= 10*big_blind(): return self.chips
                elif self.stack_position()%2 == 0 and category(self.hand) <= 3 and max(temp_pot)>= 8*big_blind(): return self.chips
                elif self.stack_position()%2 == 0 and category(self.hand) <= 4 and max(temp_pot)>= 6*big_blind(): return self.chips
                elif self.stack_position()%2 == 0 and category(self.hand) <= 5 and max(temp_pot)>= 4*big_blind(): return self.chips
                elif self.stack_position()%2 == 0 and category(self.hand) <= 8 and max(temp_pot) == big_blind(): return self.chips
                else: return 0
            elif len(table) >= 3:
                if medium_made_draw(self.hand, table): return self.chips
                else: return 0
        elif len(players) == 2:
            if len(table) == 0:
                if max(temp_pot) == big_blind():
                    if self.chips < 8*big_blind(): return self.chips
                    elif self.chips < 12*big_blind() and category(self.hand) < 8: return self.chips
                    elif self.chips >= 12*big_blind() and category(self.hand) and max(temp_pot) == big_blind() < 7: return 4*big_blind()
                    else: return 0
                elif max(temp_pot) > big_blind():
                    if self.chips < 2.5*big_blind(): return max(temp_pot) - self.current_bet
                    elif self.chips < 4*big_blind() and category(self.hand) < 8: return self.chips
                    elif self.chips < 12*big_blind() and category(self.hand) < 7: return self.chips
                    elif self.chips >= 12*big_blind() and category(self.hand) < 6: return 4*max(temp_pot)
                    else: return 0
                else: return 0
            elif len(table) >= 3:
                if medium_made_draw(self.hand, table): return self.chips
                else: return 0
        else: return 0

#Funktsioon mängu alustamiseks, kus määratakse ära chipide algkogus ja mängija nimi, tagastab mängija objekti
def game_start():
    name = input("Sisesta enda nimi")
    return Player(name, 3000, [], 0, 0, "human")

#Funktsioon vastaste listi tegemiseks
def opps():
    opponents = []
    for i in range(1, 9):
        opponents.append(Player("Vastane %i" %i, 3000, [], i, 0, "AI"))
    return opponents

pack = deck()
pot = 0
human = game_start()
opponents = opps()
players = [human]
players.extend(opponents)
temp_pot = empty_pot(len(players))
raam = gui(players, table)
game()





