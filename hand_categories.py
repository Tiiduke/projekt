__author__="Tiit Hendrik Piibeleht"


from hands_and_values import *
#Tagastab pocketi kategooria
def pocket(hand):
    if hand[0][0]==hand[1][0]:
        for i in range(0, 6):
            if hand[0][0] + 2*i > 10:
                return i + 1
            elif hand[0][0]==0:
                return 6
    else:
        return 9

def deck():
    deck = []
    for i in range(0,13):
        for j in range(0,4):
            deck.append((i, j))
    return deck
#Tagastab suited connectori kategooria
def suit_connect(hand):
    if hand[0][1]==hand[1][1] and abs(hand[0][0]-hand[1][0])==1:
        if max(hand[0][0], hand[1][0])==12:
            return 2
        elif max(hand[0][0], hand[1][0])==11:
            return 4
        elif max(hand[0][0], hand[1][0])==10:
            return 6
        elif max(hand[0][0], hand[1][0]) > 2:
            return 7
        return 9
    else: return 9
#Tagastab käe vahega suited kategooria
def suit_over_connect(hand):
    if hand[0][1]==hand[1][1] and abs(hand[0][0]-hand[1][0])==2:
        if max(hand[0][0], hand[1][0])==12:
            return 3
        elif max(hand[0][0], hand[1][0])==11:
            return 5
        elif max(hand[0][0], hand[1][0])==10:
            return 6
        elif max(hand[0][0], hand[1][0]) > 3:
            return 8
        return 9
    else: return 9
#Tagastab offsuit connectori kategooria
def offsuit_connector(hand):
    if hand[0][1]!=hand[1][1] and abs(hand[0][0]-hand[1][0])==1:
        if max(hand[0][0], hand[1][0])==12:
            return 2
        elif max(hand[0][0], hand[1][0])==11:
            return 5
        elif max(hand[0][0], hand[1][0]) > 8:
            return 7
        elif max(hand[0][0], hand[1][0]) > 4:
            return 8
        return 9
    else: return 9
#Tagastab offsuit vahega käe kategooria
def offsuit_over_connect(hand):
    if hand[0][1]!=hand[1][1] and abs(hand[0][0]-hand[1][0])==2:
        if max(hand[0][0], hand[1][0])==12:
            return 3
        elif max(hand[0][0], hand[1][0])==11:
            return 6
        elif max(hand[0][0], hand[1][0])==10:
            return 7
        return 9
    else: return 9
#Tagastab käe kategooria kui kaardid on over two connector
def over_two_connector(hand):
    if hand[0][1]==hand[1][1] and abs(hand[0][0]-hand[1][0])==3:
        if max(hand[0][0], hand[1][0])==12:
            return 4
        elif max(hand[0][0], hand[1][0])==11:
            return 6
        elif max(hand[0][0], hand[1][0])==10:
            return 8
        return 9
    elif hand[0][1]!=hand[1][1] and abs(hand[0][0]-hand[1][0])==3:
        if max(hand[0][0], hand[1][0])==12:
            return 5
        elif max(hand[0][0], hand[1][0])==11:
            return 7
        return 9
    else: return 9
#Tagastab ässa ja väikse kaardi kategooria
def ace_small_suited(hand):
    if (hand[0][0]==12 or hand[1][0]==12) and min(hand[0][0], hand[1][0]) < 8 and hand[0][1]==hand[1][1]:
        return 7
    elif max(hand[0][0], hand[1][0])==12 and min(hand[0][0], hand[1][0])==8:
        return 5
    elif max(hand[0][0], hand[1][0])==12 and min(hand[0][0], hand[1][0]) < 8 and hand[0][1]!=hand[1][1]:
        return 8
    return 9
#Tagastab kuninga või emanda ning 8 või 9 kategooria
def king_queen_mid(hand):
    if max(hand[0][0], hand[1][0])==11 and min(hand[0][0], hand[1][0]) in (6,7):
        return 8
    elif max(hand[0][0], hand[1][0])==10 and min(hand[0][0], hand[1][0]) in (6,7) and hand[0][1]==hand[1][1]:
        return 8
    else: return 9
#Tagastab käe kategooria, argument anda kahe enniku järjendina
#Nt. category([(12,0),(12,1)])
def category(hand):
    return min(king_queen_mid(hand), ace_small_suited(hand), \
               over_two_connector(hand), pocket(hand), suit_connect(hand), \
               suit_over_connect(hand), offsuit_connector(hand), \
               offsuit_over_connect(hand))

#Võtab argumendiks laua ja käe hetkeseisu ja tagastab võimaluse saada hea käsi kokku
def draw(hand, table):
    cards = hand[:]
    cards.extend(table)
    new_deck = deck()
    i = 0
    for item in new_deck:
        new_cards = cards[:]
        new_cards.append(item)
        if item not in cards and table_hand_cards(hand, table)[0] > 2:
            i += 1
    if len(cards) == 5: return 1 - (1-i/47)*(1-i/46)
    elif len(cards) == 6: return i / 46
    else: return 0

def medium_made_draw(hand, table):
    if table_hand_cards(hand, table)[0] == 1 and max(table_hand_cards(hand, table)[1])>= max(table_hand_cards(hand, table)[2]):
        return True
    elif table_hand_cards(hand, table)[0] >= 2:
        return True
    elif draw(hand, table) >= 12/(52-len(hand)-len(table)):
        return True
    else: return False

def strong_made(hand, table):
    table_values = -1
    for item in table:
        if item[0] > table_values:
            table_values = item[0]
    if table_hand_cards(hand, table)[0] >= 2: return True
    elif pocket(hand) and table_values< hand[0][0]: return True
    else: return False

def pot_odds(hand, table, bet, pot):
    if bet == 0: return True
    elif draw(hand, table) > pot/bet: return True
    else: return False