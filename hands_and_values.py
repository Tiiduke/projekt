__author__="Tiit Hendrik Piibeleht"

import itertools
#itertools.combinations(elements, number of elements in set)

#Võtab argumendiks viiest ennikust koosneva listi ja tagastab selle põhjal käe väärtuse
#formaadis (tugevus, moodustavad kaardid, kickerid)
def values_suits(hand):
    values = []
    suits = set()

    for card in hand:
        values.append(card[0])
        suits.add(card[1])
        values.sort()

    values = values[::-1]
    
    def hand_str(values):
        väärtused = set()
        kicker = []
        for element in values:
            if values.count(element) != 1:
                väärtused.add(element)
            elif values.count(element) == 1:
                kicker.append(element)
                kicker.sort()
        #Maja puhul on erand, et kickerit pole, kuid väärtuste hulgas on liikmeid
        #Sel juhul tagastab enniku teise liikmena järjendi, kus esimene liige on nõrgema kaardi arv
        if len(väärtused) == 2 and len(kicker) == 0:
            if values.count(values[0]) == 3:
                return väärtused, [2,1]
            else:
                return väärtused, [0,1]
        return väärtused, kicker

    def rida(values):
        if values == [12, 3, 2, 1, 0]:
            return True
        for i in range(0,4):
            if values[i] - 1 == values[i+1]: pass
            else: return False
        return True

    c_len = len(hand_str(values)[0])
    k_len = len(hand_str(values)[-1])
    cards = hand_str(values)[0]
    kicker = hand_str(values)[1]

    if rida(values) and len(suits) == 1 and values[0] == 8:
        return (9, 0)
    elif rida(values) and len(suits) == 1:
        return (8, kicker, kicker)
    elif c_len == 1 and k_len == 1:
        return (7, cards, kicker)
    elif c_len == 2 and k_len == 2:
        return (6, cards, kicker)
    elif len(suits) == 1:
        return (5, kicker, kicker)
    elif rida(values):
        if values == [0, 1, 2, 3, 12]: return (4, [-1, -1, -1, -1, -1])
        else: return (4, values, values)
    elif c_len == 1 and k_len == 2:
        return (3, cards, kicker)
    elif c_len == 2 and k_len == 1:
        return (2, cards, kicker)
    elif c_len == 1 and k_len == 3:
        return (1, cards, kicker)
    else:
        return (0, kicker, kicker)

#Võtab argumeniks suvalise suurusega käte väärtuste järjendi funktsioonist values_suits
#ja tagastab nendest parima käe väärtuse NB! Ei võta argumendiks kätt ennast
def better_hand(hands):
    best = -1
    for item in hands:
        if item[0] > best:
            best_hand = item
            best = item[0]
        elif item[0] == best and item[1] != best_hand[1]:
            if max(set(item[1])-set(best_hand[1])) > max(set(best_hand[1])):
                best_hand = item
        elif item[0] == best and item[1] == best_hand[1] and item[2] != best_hand[2]:
            if max(set(item[2])-set(best_hand[2])) > max(set(best_hand[2])):
                best_hand = item
    return best_hand

#Võtab argumendiks mingi hulga kaarte ja tagastab neist kõrgeima väärtuse
def highest_card(cards):
    a = -1
    for item in cards:
        if item[0] > a: a = item[0]
    return a

#Võtab argumendiks n-kaardise kombinatsiooni ja tagastab tugevaima viiekaardise kombinatsiooni väärtuse
def table_hand_cards(hand, table):
    cards = hand[:]
    cards.extend(table)
    hands = list(itertools.combinations(cards, 5))
    hand_values = []
    for item in hands:
        hand_values.append(values_suits(item))
    return better_hand(hand_values)
