Projekt: Pokkerim�ng
Projekti autorid: Ander Peedum�e ja Tiit Hendrik Piibeleht

Elemendid:
K�te jagamine/lauakaardid
	v List, kus on kaardid - sealt valib randomilt ja eemaldab �he peale iga kaardi jagamist
K�te paremuse hindamine
	Vastaste k�ed peavad olema varjatud kuni l�puni
	v Parim k�si v�idab poti, spliti korral jaotumine
	v Parima k�e leidmine koos lauakaartidega
Blindide/dealeri liikumine
	v Erinev roll iga k�si
Chipide jaotamine/panustamine
	v Side potid
	v Chipside eemaldamine kuhjast peale panustamist
Tingimuslaused(AI)
	v K�te paremusarvestuse list
	v Rolli m��ramine ja panuste tegemine arvestades neid tingimusi
M�ngu tempo
	v Blindide algsuurus ja suurenemise kiirus

UI:
Algus: m�ngu tempo, vastaste arv, chipside hulk, ante, blindid
Kujundus laua ja vastaste jaoks(kaardid)
Nupud otsuse tegemiseks(Fold/Check/Call/Raise)
Lauakaardid peavad olema interfaces n�htavad

Asjade j�rjestus:

Algus: display meie kaardid+vastaste cardbackid, dealer/SB/BB staatus, algpanuste ja poti suurus
Betting: muutuvad ainult summad(panus m�ngija ees+total poti suurus), kaartidega midagi ei juhtu
Tulevad kolm lauakaarti: peavad n�htavad olema
Betting:
Tuleb �ks lauakaart:
Betting:
Tuleb �ks lauakaart:
Betting:
Showdown: vastaste kaardid tulevad n�htavale, kui ta k�si on tugevam, kui hetke tugevaim(k�ik kaardid mis on mingil hetkel olnud tugevaimad, j��vad n�htavale)
